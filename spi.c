
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>


int main()
{
	int err_code;

	printf("SPI Init\n");
	err_code = wiringPiSPISetup(0,500); // channel and speed
	if(err_code == -1) printf("Erro na inicializacao da SPI");

	unsigned char data;
	data = 55;

	while(1)
	{
		wiringPiSPIDataRW (0, &data, 1);
		delay(500);
	}
}
