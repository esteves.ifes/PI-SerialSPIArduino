#define CLK_    17
#define MISO_   16
#define MOSI_   15

int state       = 0;
int last_state  = 0;
int counter     = 0;
char data_in    = 0;
char data_out   = 0;
char aux        = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(CLK_, INPUT);
  pinMode(MISO_, OUTPUT);
  pinMode(MOSI_, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

  last_state = state;
  state = digitalRead(CLK_);

  // Rising edge
  if( (state == 1) && (last_state == 0) )
  {
    //Serial.println("Rising edge");
    aux = digitalRead(MOSI);
    Serial.println(aux,DEC);
    data_in <<= 1;
    data_in |= (aux & 1);    
    counter++;

    if( counter == 8 )
    {
      Serial.print("Valor Recebido");
      Serial.println(data_in);
      counter = 0;   
    }
  }

}
