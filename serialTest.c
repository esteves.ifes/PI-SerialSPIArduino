/*
 * serialTest.c:
 *	Very simple program to test the serial port. Expects
 *	the port to be looped back to itself
 *
 * Copyright (c) 2012-2013 Gordon Henderson. <projects@drogon.net>
 ***********************************************************************
 * This file is part of wiringPi:
 *	https://projects.drogon.net/raspberry-pi/wiringpi/
 *
 *    wiringPi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    wiringPi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with wiringPi.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringSerial.h>

int main ()
{
  int fd ;
  int count ;
  unsigned long int nextTime, nextTime2;
  int on_off_flag = 0;

  if ((fd = serialOpen ("/dev/ttyAMA0", 115200)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }

  nextTime   = millis();
  nextTime2  = millis();

// #### MAIN LOOP ####

  printf("LET THE GAME BEGIN\n\r");

  while(1)
  {
    
     // #### Envia comando para ligar e desligar o led ####
     if( millis() > nextTime + 500 )
     {
        nextTime = millis();

		if( on_off_flag == 0 )
		{
			//printf("A\n\r");
			fflush(stdout);
			serialPutchar(fd,'a');
			on_off_flag = 1;
		}
		else
		{
			//printf("B\n\r");
			fflush(stdout);
			serialPutchar(fd,'b');
			on_off_flag = 0;
		}
    }

    // #### Se tiver dado disponivel da porta serial printa na tela ####
    putchar(serialGetchar (fd));
    fflush(stdout);

     // #### Checa estado do botao na TEENSY a cada 100 mili segundos ####	  
     if( millis() > nextTime2 + 100 )
     {
        nextTime2 = millis();
		fflush(stdout);
		serialPutchar(fd,'c');
     }	     

  }

  return 0 ;
}
